﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Net.NetworkInformation;
using System.Numerics;
using System.Text;
using System.Threading;
using System.Linq;
using System.IO;

namespace TraceRoute
{
    /// <summary>
    /// Результат промежуточного узла
    /// </summary>
    public class HopResult
    {
        public int number; // Номер
        public String targetIP;    // IP-адрес узла
        public String targetURL;    // URL узла
        public long elapsed; // Пинг в мс
        public IPStatus status; // Статус подключения

        public void dump()
        {
            Console.WriteLine("HopResult: \n#{0}\n\tTargetIP: {1}\n\tTargetURL: {2}\n\tElapsed: {3}\n\tStatus: {4}", number, targetIP, targetURL, elapsed, status);
        }
    }


    class Tracert{

        protected static byte[] DataPacket = Encoding.ASCII.GetBytes("abcdefghijklmnopqrstuvwxyzabcdef");


        protected bool threadStopped = false;
        protected static Mutex consoleMutex = new Mutex();
        protected static Mutex fileMutex = new Mutex();
        protected Thread thread;

        public void TraceTargetInNewThread(String target, int timeout = 10, int retryCount = 4)
        {
            thread = new Thread(() => StartTraceInThread(target, timeout, retryCount));
            thread.Start();
        }

        public void stopThread()
        {
            threadStopped = true;
        }
        

        protected void StartTraceInThread(String target, int timeout = 10, int retryCount = 4)
        {
            while (!threadStopped)
            {
                Tracert tracert = new Tracert();
                tracert.TraceTarget(target, timeout, retryCount);
                Thread.Sleep(180 * 1000);
            }
        }

        //protected String 
        /// <summary>
        /// 
        /// </summary>
        /// <param name="target">The URL or IP we target to</param>
        /// <param name="timeout"></param>
        /// <param name="retryCount">Количество подходов к трассировке узла</param>
        public List<HopResult> TraceTarget(String target, int timeout = 10, int retryCount = 4)
        {
            List<HopResult> hopsList = new List<HopResult>();

            timeout *= 1000; // Перевод в миллисекунды
            
            int hopNumber = 1;  // Количество узлов
            // Основной цикл по узлам
            while (true)
            {
                HopResult hop = new HopResult();
                hop.number = hopNumber;


                // Инициализация основных классов
                Ping pinger = new Ping();   
                PingOptions pingerOptions = new PingOptions(hop.number, true);   // Учет номера узла, на котором остановились
                PingReply pingResponse;

                int tries = 0;
                long elapsed;
                // Цикл по количеству попыток доступа к цели
                do
                {
                    Stopwatch timer = Stopwatch.StartNew();
                    pingResponse = pinger.Send(target, timeout, DataPacket, pingerOptions);
                    timer.Stop();
                    elapsed = timer.ElapsedMilliseconds;    // Миллисекунд прошло
                    hop.elapsed = elapsed;

                    if (pingResponse.Status == IPStatus.TtlExpired ||
                        pingResponse.Status == IPStatus.TimeExceeded ||
                        pingResponse.Status == IPStatus.Success)
                        break;
                    

                    tries++;
                } while (tries < retryCount);
                

                hop.targetIP = pingResponse.Address.ToString();

                // Если удалось достичь узла
                if (pingResponse.Status == IPStatus.Success)
                {
                    hop.elapsed = pingResponse.RoundtripTime;

                    break;
                }
                else if (pingResponse.Status == IPStatus.TimeExceeded || pingResponse.Status == IPStatus.TtlExpired)
                {
                    PingReply intermediateResponse;
                    int subtries = 0;
                    do
                    {
                        intermediateResponse = pinger.Send(pingResponse.Address, timeout, DataPacket);
                        subtries++;

                        if (intermediateResponse.Status == IPStatus.Success) {
                            break;
                        }
                        
                    } while (subtries < retryCount);
                    hop.elapsed = intermediateResponse.RoundtripTime;
                }
                else
                {
                    hop.status = pingResponse.Status;
                    hop.elapsed = elapsed;
                }
                hop.targetURL = GetHostName(pingResponse.Address.ToString());

                
                hopsList.Add(hop);  // Записываем узел в список
                
                
                consoleMutex.WaitOne();
                hop.dump(); // Вывод всей инфы о хопе
                consoleMutex.ReleaseMutex();

                // CSV Logging
                var csv = new StringBuilder();
                var newLine = string.Format("{0};{1};{2};{3};{4};{5}", target, hopNumber, hop.targetIP, hop.targetURL, hop.status, hop.elapsed);
                csv.AppendLine(newLine);
                fileMutex.WaitOne();    // LOCK MUTEX
                File.AppendAllText("log.csv", csv.ToString());
                fileMutex.ReleaseMutex();   // RELEASE MUTEX

                hopNumber++;    // Следующий узел 
            }

            return hopsList;

        }


        
        /// <summary>
        /// Prints the hostname
        /// </summary>
        /// <param name="IPAddress">The IP address</param>
        public static void PrintHostName(string IPAddress)
        {
            try
            {
                string hostName = Dns.GetHostEntry(IPAddress).HostName;
                //if (Program.Options["bare"] == "false")
                    Console.Write(String.Format("<{0}>", hostName));
                //else
                //   Console.Write(hostName);
            }
            catch (Exception error)
            {
                string shortMessage = "";
                if (error.Message.Contains("no data of the requested type was found"))
                {
                    shortMessage = "No HostName Found";
                }
                else
                {
                    shortMessage = error.Message;
                }
                Console.Write(String.Format("({0})", shortMessage));
            }
            Console.Write(" ");
        }

        public String GetHostName(String IPAddress)
        {
            string hostName;
            try
            {
                hostName = Dns.GetHostEntry(IPAddress).HostName;
            }
            catch (Exception error)
            {
                hostName = "";
            }
            return hostName;
        }
    }
}
