﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading;

namespace TraceRoute
{
    class Program
    {
        static void Main(string[] args)
        {

            var csv = new StringBuilder();
            var newLine = string.Format("{0};{1};{2};{3};{4};{5}", "Target:", "Hop Number", "Target IP", "Target URL", "Status", "Time elapsed");
            csv.AppendLine(newLine);
            File.WriteAllText("log.csv", csv.ToString());

            Tracert tracert = new Tracert();
            tracert.TraceTargetInNewThread("vk.com");

            Tracert tracert2 = new Tracert();
            tracert.TraceTargetInNewThread("google.com");

            Tracert tracert3 = new Tracert();
            tracert.TraceTargetInNewThread("facebook.com");

            Console.WriteLine("Hello World!");

            Console.ReadKey();
        }
    }
}
